(function() {
    var viewModel = {};

    $.getJSON("assets/js/ResumeData.json", function(jsonData) {
        viewModel = jsonData;
        initUI();
    });

    function initUI() {

        if (viewModel) {

            viewModel.Age = CalculateAge(viewModel.BirthDate);
            CalculateYearsExperience(viewModel.Experience);
            CalculateSkillWidth(viewModel.Skills);

            viewModel.Skills.sort(function(a, b) {
                return b.Level - a.Level;
            })

            try {
                $(".photo").load("https://twitter.com/" + viewModel.Contact.Twitter + " .ProfileAvatar-image");
                ko.applyBindings(viewModel);
                var callback = function() {
                    $('.item-skills').each(function() {
                        newWidth = $(this).parent().width() * $(this).data('percent');
                        $(this).width(0);
                        $(this).animate({
                            width: newWidth,
                        }, 3000);
                    });
                    $('.icons-red').each(function() {
                        height = $(this).height();

                        $(this).animate({
                            height: 14,
                        }, 3000);
                    });
                };
                $(document).ready(callback);

            } catch (err) {
                console.log(err);
            }
        }
    }

    function CalculateSkillWidth(skills) {
        var maxWidth = $(".skills-legend").width();
        for (var indexItem in skills) {
            var item = skills[indexItem];
            item.Width = (item.Level * maxWidth) / 5;
            item.Percent = item.Level / 5;
        }
    }


    function CalculateAge(birthDate) {
        var now = Date.today();
        var ageDate = new Date(now - birthDate);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    function CalculateYearsExperience(historyExperience) {
        for (var indexCompany in historyExperience) {
            var months = 0;
            var company = historyExperience[indexCompany];
            for (var indexProject in company.Projects) {
                var project = company.Projects[indexProject];
                months += parseInt(project.Duration.split(" ")[0]);
            }
            company.Months = months % 12;
            company.Years = Math.trunc(months / 12);

        }
    }

})();
